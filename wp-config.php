<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'luxcom_magic');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'JR8MTzN_GtpMt+T@/#e|30.%z|#>16M?&Kwk!*>Cllhhi[2hLPR.z>O* wP`xtNe');
define('SECURE_AUTH_KEY',  'aAgO,HSG5}#f3OO=&v{):pOx4|GzKHph~(kjUaS1}LwaRc XzqeR>)PisLS8} f=');
define('LOGGED_IN_KEY',    'D{5]9{B4BpSgC{s!%jaQ;bM}dtm:V21z43#wG{lj5w9Do#Yi6G}Af+B#$L-@2%Z]');
define('NONCE_KEY',        'dbFt5/>I-^gB-C$^t+?$U(5hm%aAb%~cg(cJEdF/[u8CERbrzGX/LSzuMx],1&$E');
define('AUTH_SALT',        'jWE[IGTo*d)-^`tps^<4mS#F_>trpbK(yn4D#*ZXzLbljj0!k2jxk>t{S-11<AA9');
define('SECURE_AUTH_SALT', 'Z:gC{l2<PrPSx(>$*JlfZLt9k2;t8P{J>y4z&fq |c?enN[,CSc:f>!*$ub+ugR+');
define('LOGGED_IN_SALT',   'y|gCkJ}Foyd>5CDk1mMUUX-*L(_-3EU9Emjxed#~*@t.UvAzk:Xei`m>.z)OyvXT');
define('NONCE_SALT',       'M#-lhVP&u?Hx6<CFOQaj=_I4E4d,M44#Mt?nuTy<N )|.q[(KPdw;_veN6GYL!~z');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
