<div class="x-navbar-wrap">
    <div class="x-navbar">
        <div class="x-navbar-inner">
            <div class="x-container max width">

                <a href="#" class="x-btn-navbar collapsed" data-toggle="collapse"
                   data-target=".x-nav-wrap.mobile">
                    <i class="x-icon-bars" data-x-icon="&#xf0c9;"></i>
                    <span class="visually-hidden">Navigation</span>
                </a>

                <nav class="x-nav-wrap desktop" role="navigation">
                    <ul id="menu-menu-glowne" class="x-nav">
                        <li id="menu-item-14"
                            class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-6 current_page_item menu-item-14">
                            <a href="default.htm"><span>Home</span></a></li>
                        <li id="menu-item-13"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13"><a
                                href="produkty/default.htm"><span>Produkty</span></a></li>
                        <li id="menu-item-439"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-439"><a
                                href="realizacje/default.htm"><span>Realizacje</span></a></li>
                        <li id="menu-item-308"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-308"><a
                                href="do-pobrania/default.htm"><span>Do&nbsp;pobrania</span></a></li>
                        <li id="menu-item-443"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-443"><a
                                href="dystrybutorzy/default.htm"><span>Dystrybutorzy</span></a></li>
                        <li id="menu-item-582"
                            class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-582 tax-item tax-item-1">
                            <a title="Blog" href="blog/default.htm"><span>Blog</span></a></li>
                        <li id="menu-item-27"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27"><a
                                href="kontakt/default.htm"><span>Kontakt</span></a></li>
                        <li class="menu-item x-menu-item x-menu-item-search"><a href="#"
                                                                                class="x-btn-navbar-search"><span><i
                                        class="x-icon-search" data-x-icon="&#xf002;"></i><span
                                        class="x-hidden-desktop"> Search</span></span></a></li>
                    </ul>
                </nav>

                <div class="x-nav-wrap mobile collapse">
                    <ul id="menu-menu-glowne-1" class="x-nav">
                        <li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-6 current_page_item menu-item-14">
                            <a href="default.htm"><span>Home</span></a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13"><a
                                href="produkty/default.htm"><span>Produkty</span></a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-439"><a
                                href="realizacje/default.htm"><span>Realizacje</span></a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-308"><a
                                href="do-pobrania/default.htm"><span>Do&nbsp;pobrania</span></a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-443"><a
                                href="dystrybutorzy/default.htm"><span>Dystrybutorzy</span></a></li>
                        <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-582 tax-item tax-item-1">
                            <a title="Blog" href="blog/default.htm"><span>Blog</span></a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27"><a
                                href="kontakt/default.htm"><span>Kontakt</span></a></li>
                        <li class="menu-item x-menu-item x-menu-item-search"><a href="#"
                                                                                class="x-btn-navbar-search"><span><i
                                        class="x-icon-search" data-x-icon="&#xf002;"></i><span
                                        class="x-hidden-desktop"> Search</span></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>