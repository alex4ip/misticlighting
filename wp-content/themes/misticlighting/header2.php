<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package misticlighting
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!--home page page-id-2 page-template-default logged-in admin-bar wpb-js-composer js-comp-ver-4.12 vc_responsive customize-support cookies-not-accepted-->
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'misticlighting' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<?php
			if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php
			endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'misticlighting' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->
		
		
		
		
<!--		**********-->
		<header class="masthead masthead-stacked" role="banner">


			<div class="x-logobar">
				<div class="x-logobar-inner">
					<div class="x-container max width">
						<!--					<h1 class="visually-hidden">Mistic Lighting</h1>-->
						<h1 class="visually-hidden"><?php bloginfo('name'); ?> </h1>
						<a href="<?php echo get_option('home'); ?>/" class="x-brand img" title="Bring litght to life">
							<img src="<?php bloginfo('template_directory'); ?>/img/mistic-logo.png" alt="Bring litght to life"></a></div>
				</div>
			</div>

			<div class="x-navbar-wrap">
				<div class="x-navbar">
					<div class="x-navbar-inner">
						<div class="x-container max width">

							<a href="#" class="x-btn-navbar collapsed" data-toggle="collapse"
							   data-target=".x-nav-wrap.mobile">
								<i class="x-icon-bars" data-x-icon="&#xf0c9;"></i>
								<span class="visually-hidden">Navigation</span>
							</a>

							<nav class="x-nav-wrap desktop" role="navigation">
								<ul id="menu-menu-glowne" class="x-nav">
									<li id="menu-item-14"
									    class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-6 current_page_item menu-item-14">
										<a href="default.htm"><span>Home</span></a></li>
									<li id="menu-item-13"
									    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13"><a
											href="produkty/default.htm"><span>Produkty</span></a></li>
									<li id="menu-item-439"
									    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-439"><a
											href="realizacje/default.htm"><span>Realizacje</span></a></li>
									<li id="menu-item-308"
									    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-308"><a
											href="do-pobrania/default.htm"><span>Do&nbsp;pobrania</span></a></li>
									<li id="menu-item-443"
									    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-443"><a
											href="dystrybutorzy/default.htm"><span>Dystrybutorzy</span></a></li>
									<li id="menu-item-582"
									    class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-582 tax-item tax-item-1">
										<a title="Blog" href="blog/default.htm"><span>Blog</span></a></li>
									<li id="menu-item-27"
									    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27"><a
											href="kontakt/default.htm"><span>Kontakt</span></a></li>
									<li class="menu-item x-menu-item x-menu-item-search"><a href="#"
									                                                        class="x-btn-navbar-search"><span><i
													class="x-icon-search" data-x-icon="&#xf002;"></i><span
													class="x-hidden-desktop"> Search</span></span></a></li>
								</ul>
							</nav>

							<div class="x-nav-wrap mobile collapse">
								<ul id="menu-menu-glowne-1" class="x-nav">
									<li class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-6 current_page_item menu-item-14">
										<a href="default.htm"><span>Home</span></a></li>
									<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13"><a
											href="produkty/default.htm"><span>Produkty</span></a></li>
									<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-439"><a
											href="realizacje/default.htm"><span>Realizacje</span></a></li>
									<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-308"><a
											href="do-pobrania/default.htm"><span>Do&nbsp;pobrania</span></a></li>
									<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-443"><a
											href="dystrybutorzy/default.htm"><span>Dystrybutorzy</span></a></li>
									<li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-582 tax-item tax-item-1">
										<a title="Blog" href="blog/default.htm"><span>Blog</span></a></li>
									<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27"><a
											href="kontakt/default.htm"><span>Kontakt</span></a></li>
									<li class="menu-item x-menu-item x-menu-item-search"><a href="#"
									                                                        class="x-btn-navbar-search"><span><i
													class="x-icon-search" data-x-icon="&#xf002;"></i><span
													class="x-hidden-desktop"> Search</span></span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

		</header>
		
		
		
	</header><!-- #masthead -->

	<div id="content" class="site-content">
