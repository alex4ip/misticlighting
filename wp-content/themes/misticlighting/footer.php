<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package misticlighting
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
<!--		<div class="site-info">-->
<!--			<a href="--><?php //echo esc_url( __( 'https://wordpress.org/', 'misticlighting' ) ); ?><!--">--><?php //printf( esc_html__( 'Proudly powered by %s', 'misticlighting' ), 'WordPress' ); ?><!--</a>-->
<!--			<span class="sep"> | </span>-->
<!--			--><?php //printf( esc_html__( 'Theme: %1$s by %2$s.', 'misticlighting' ), 'misticlighting', '<a href="http://underscores.me/" rel="designer">Underscores.me</a>' ); ?>
<!--		</div><!-- .site-info -->-->

		<footer class="x-colophon bottom" role="contentinfo">
			<div class="x-container max width">

				<div class="x-social-global"><a href="../https@www.facebook.com/misticlighting" class="facebook"
				                                title="Facebook" target="_blank"><i class="x-icon-facebook-square"
				                                                                    data-x-icon="&#xf082;"></i></a><a
						href="../https@twitter.com/misticlighting" class="twitter" title="Twitter" target="_blank"><i
							class="x-icon-twitter-square" data-x-icon="&#xf081;"></i></a><a
						href=" ../instagram.com/misticlighting" class="instagram" title="Instagram" target="_blank"><i
							class="x-icon-instagram" data-x-icon="&#xf16d;"></i></a><a
						href="../https@www.pinterest.com/misticlighting/default.htm" class="pinterest" title="Pinterest"
						target="_blank"><i class="x-icon-pinterest-square" data-x-icon="&#xf0d3;"></i></a></div>

				<ul id="menu-menu-w-stopce" class="x-nav">
					<li id="menu-item-312"
					    class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-6 current_page_item menu-item-312">
						<a href="default.htm">Home</a></li>
					<li id="menu-item-311" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-311"><a
							href="produkty/default.htm">Produkty</a></li>
					<li id="menu-item-438" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-438"><a
							href="realizacje/default.htm">Realizacje</a></li>
					<li id="menu-item-309" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-309"><a
							href="do-pobrania/default.htm">Do&nbsp;pobrania</a></li>
					<li id="menu-item-316" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-316"><a
							href="polityka-prywatnosci/default.htm">Polityka prywatności</a></li>
					<li id="menu-item-444" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-444"><a
							href="dystrybutorzy/default.htm">Dystrybutorzy</a></li>
					<li id="menu-item-310" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-310"><a
							href="kontakt/default.htm">Kontakt</a></li>
				</ul>
				<div class="x-colophon-content">
					<p style="letter-spacing: 2px; text-transform: uppercase; opacity: 0.5; filter: alpha(opacity=50);">
						Copyright © 2014-2015 Mistic Lighting</p></div>

			</div>
		</footer>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
